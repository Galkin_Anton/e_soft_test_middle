#!/usr/bin php7.0
<?php
header('Content-Type: text/plain');
error_reporting(E_ALL);
ini_set("log_errors", 1);
ini_set("error_log", "logs/server-error.log");
error_log( "Start server!" );

include "settings/db.php";
include "php/dbfunc.php";

/* Позволяет скрипту ожидать соединения бесконечно. */
set_time_limit(0);

/* Включает скрытое очищение вывода так, что мы получаем данные
 * как только они появляются. */
ob_implicit_flush();

$address = '0.0.0.0';
$port = 8001;
$null = NULL;


if (($socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
    error_log("Не удалось выполнить socket_create(): причина: " . socket_strerror(socket_last_error()));
}

if (socket_bind($socket, $address, $port) === false) {
    error_log("Не удалось выполнить socket_bind(): причина: " . socket_strerror(socket_last_error($socket)));
}

if (socket_listen($socket, 5) === false) {
    errpr_log("Не удалось выполнить socket_listen(): причина: " . socket_strerror(socket_last_error($socket)));
}
//создаем список
$clients = array($socket);
//Создаю массив с сокетами и пользователями
//Идея в том что вначале идет сокет на следуюющей позиции имя пользователя(имя пользователя уникально!)
$socket_users = [];

do {
    //несколько клиентов
    $changed = $clients;
    
    socket_select($changed, $null, $null, 0, 10);
	//проверка на новые сокеты
	if (in_array($socket, $changed)) {
		$socket_new = socket_accept($socket); //принимаем новый сокет
		$clients[] = $socket_new; //добавляем в массив
		$header = socket_read($socket_new, 1024); //считываем данные указанные в header
		

		perform_handshaking($header, $socket_new, $address, $port, $user_name); //формируем ответ хедеров
		//Добавляю в список сокет и имя пользователя
		$socket_users[] = $socket_new;
		$socket_users[] = $user_name;

		socket_getpeername($socket_new, $ip); //ip адрес клиента
		//Отправим сообщение
		$response = mask(json_encode(array('type'=>'all','name' => $user_name, 'message'=>get_user_list()))); //подготавливаем json данные

		send_message($response); //говорим всем пользователям о новом участнике
		
		//make room for new socket
		$found_socket = array_search($socket, $changed);
		unset($changed[$found_socket]);
	}

    //Для всех сокетов
    foreach ($changed as $changed_socket) {	
		
		//Проверяем на любые входящие сообщения
		while(socket_recv($changed_socket, $buf, 1024, 0) >= 1)
		{
			$received_text = unmask($buf); //unmask data
			$tst_msg = json_decode($received_text); //json decode 
			$user_name = $tst_msg->name; //sender name
			$user_message = $tst_msg->message; //message text
			$user_color = $tst_msg->color; //color
			$user_id = $tst_msg->userId;
			$room_id = $tst_msg->roomId;
			$user_for = $tst_msg->userFor;
			$date_time = $tst_msg->dateTime;
			$type = $tst_msg->type;
			
			//Записываем в базу данных (./php/dbfunc)
			if($user_message && $type == 'usermsg') {
				insert_message($user_message, $user_id, $room_id, $date_time, $user_for);

				$response_text = mask(json_encode(array('type'=>$type, 'name'=>$user_name, 'message'=>$user_message, 'color'=>$user_color, 'userId' => $user_id,
					'dateTime' => $date_time,
					'userFor' => $user_for
					)));
				send_message($response_text); //send data
			}
			break 2;
		}
		
		$buf = @socket_read($changed_socket, 1024, PHP_NORMAL_READ);
		if ($buf === false) { // check disconnected client
			// убираем клиентов
			$found_socket = array_search($changed_socket, $clients);
			socket_getpeername($changed_socket, $ip);
			unset($clients[$found_socket]);

			$found_socket = array_search($changed_socket, $socket_users);
			
			$user_name = $socket_users[$found_socket + 1];
			
			unset($socket_users[$found_socket + 1]);
			unset($socket_users[$found_socket]);
			//Сообщаем всем что пользователь вышел
			//Проверяем есть ли еще сокеты на данном пользователе
			if (!in_array($user_name, $socket_users)) {
				$response = mask(json_encode(array('type'=>'out', 'name' => $user_name, 'message'=>$ip.' disconnected')));
				send_message($response);
			}
		}
	}
    

} while (true);

socket_close($socket);



//отравка сообщения всем клиентам
function send_message($msg)
{
	global $clients;
	foreach($clients as $changed_socket)
	{
		@socket_write($changed_socket,$msg,strlen($msg));
	}
	return true;
}

//handshake new client.
function perform_handshaking($receved_header, $client_conn, $host, $port, &$user_name)
{
	$headers = array();
	$lines = preg_split("/\r\n/", $receved_header);
	foreach($lines as $line)
	{
		$line = chop($line);
		if(preg_match('/\A(\S+): (.*)\z/', $line, $matches))
		{
			$headers[$matches[1]] = $matches[2];
		}
	}

	$secKey = $headers['Sec-WebSocket-Key'];
	$user_name = get_socket_user_name($headers['Cookie']);
	$secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));

	$upgrade  = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
	"Upgrade: websocket\r\n" .
	"Connection: Upgrade\r\n" .

	"Sec-WebSocket-Accept:$secAccept\r\n\r\n";
	socket_write($client_conn,$upgrade,strlen($upgrade));
}

//Encode message for transfer to client.
function mask($text)
{
	$b1 = 0x80 | (0x1 & 0x0f);
	$length = strlen($text);
	
	if($length <= 125)
		$header = pack('CC', $b1, $length);
	elseif($length > 125 && $length < 65536)
		$header = pack('CCn', $b1, 126, $length);
	elseif($length >= 65536)
		$header = pack('CCNN', $b1, 127, $length);
	return $header.$text;
}

//Unmask incoming framed message
function unmask($text) {
	if(!$text) return;
	$length = ord($text[1]) & 127;
	if($length == 126) {
		$masks = substr($text, 4, 4);
		$data = substr($text, 8);
	}
	elseif($length == 127) {
		$masks = substr($text, 10, 4);
		$data = substr($text, 14);
	}
	else {
		$masks = substr($text, 2, 4);
		$data = substr($text, 6);
	}
	$text = "";
	for ($i = 0; $i < strlen($data); ++$i) {
		$text .= $data[$i] ^ $masks[$i%4];
	}
	return $text;
}

//Забираю имя пользователя
function get_socket_user_name($str) {
	$mas = explode(';', $str);
	foreach ($mas as $key => $value) {
		
		if(strrpos($value, 'chatuser')) return explode('=',$value)[1];
	}
	return null;
}

function get_user_list() {
	global $socket_users;
	$res = [];
	//Бежим по списку и выбираем пользователей
	foreach ($socket_users as $key => $value) {
		if (gettype($value) == 'string') $res[] = $value;
	}	
	return json_encode($res);
}
?>