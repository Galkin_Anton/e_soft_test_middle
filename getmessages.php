<?php
error_reporting(E_ALL);
ini_set("log_errors", 1);
ini_set("error_log", "/logs/messages-error.log");
include "settings/db.php";
include "php/dbfunc.php";

$data = get_messages(100);

if($data === false) {
    header('HTTP/1.1 503 ERR');
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array('message' => 'Нет доступа к истории', 'code' => 503)));
}

echo json_encode($data);
	
?>