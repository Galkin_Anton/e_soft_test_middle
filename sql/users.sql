CREATE TABLE users (
   u_id               integer        NOT NULL,
   u_name             varchar(40)    NOT NULL,
   u_pass             varchar(100)   NOT NULL,
   u_hash             varchar(200)   NOT NULL,
   u_date_create      timestamp,
   u_date_last_login  timestamp,
   u_color character varying(50)-- цвет сообщений
);

ALTER TABLE users
   ADD CONSTRAINT fk_users
   PRIMARY KEY (u_id);

-- Index: public.u_index_1

-- DROP INDEX public.u_index_1;

CREATE UNIQUE INDEX u_index_1
  ON public.users
  USING btree
  (u_id);
-- Index: public.u_index_2

-- DROP INDEX public.u_index_2;

CREATE UNIQUE INDEX u_index_2
  ON public.users
  USING btree
  (u_name COLLATE pg_catalog."default");
/*SEQUENCESQ*/

CREATE SEQUENCE u_seq INCREMENT BY 1 START WITH 1;

/*TRIGGER FUNCTION*/
CREATE OR REPLACE FUNCTION users_before_ins () RETURNS trigger AS  $$
BEGIN 
NEW.u_id=nextval('u_seq');
NEW.u_date_create=current_timestamp;
NEW.u_date_last_login=current_timestamp;
return NEW;
END; 
$$ LANGUAGE  plpgsql;


-- Создание триггера
DROP TRIGGER IF EXISTS users_bi ON users;

CREATE TRIGGER users_bi 
BEFORE INSERT ON users FOR EACH ROW 
EXECUTE PROCEDURE users_before_ins ();