-- Table: public.messages

-- DROP TABLE public.messages;

CREATE TABLE public.messages
(
  m_id integer NOT NULL,
  m_message character varying(6000) NOT NULL,
  m_user_id integer NOT NULL,
  m_room_id integer,
  m_date_message timestamp with time zone NOT NULL,
  m_user_for integer, -- Для кого сообщение
  CONSTRAINT pk_messages PRIMARY KEY (m_id),
  CONSTRAINT fk_message_1 FOREIGN KEY (m_user_id)
      REFERENCES public.users (u_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_message_2 FOREIGN KEY (m_user_for)
      REFERENCES public.users (u_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.messages
  OWNER TO chat;
COMMENT ON COLUMN public.messages.m_user_for IS 'Для кого сообщение';


-- Index: public.m_index_1

-- DROP INDEX public.m_index_1;

CREATE UNIQUE INDEX m_index_1
  ON public.messages
  USING btree
  (m_id);

-- Index: public.m_index_2

-- DROP INDEX public.m_index_2;

CREATE INDEX m_index_2
  ON public.messages
  USING btree
  (m_id DESC, m_user_id);

-- Index: public.m_index_3

-- DROP INDEX public.m_index_3;

CREATE INDEX m_index_3
  ON public.messages
  USING btree
  (m_id DESC, m_user_for);


--sequence
CREATE SEQUENCE m_seq INCREMENT BY 1 START WITH 1;

/*TRIGGER FUNCTION*/
CREATE OR REPLACE FUNCTION messages_before_ins () RETURNS trigger AS  $$
BEGIN 
NEW.m_id=nextval('m_seq');
return NEW;
END; 
$$ LANGUAGE  plpgsql;


-- Создание триггера
DROP TRIGGER IF EXISTS messages_bi ON messages;

CREATE TRIGGER messages_bi 
BEFORE INSERT ON messages FOR EACH ROW 
EXECUTE PROCEDURE messages_before_ins ();