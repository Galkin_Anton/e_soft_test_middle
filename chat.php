<?php
//header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("log_errors", 1);
ini_set("error_log", "logs/chat-error.log");
include "settings/db.php";
include "php/dbfunc.php";

$name = $_COOKIE['chatuser'];
$hash = $_COOKIE['chathash'];


if ($name && isset($_POST) && $_POST['action']) {
	//достаем из куки данные
	$action = $_POST['action'];

	switch ($action) {
		case 'get_user':
				$data = get_user_by_hash($name, $hash);
				
                
			break;
		    default:
				//обновляем данные
				$hash = update_user_by_hash($name, $hash, $_POST['color']);
                
			break;
	}
    if (strrpos($hash, 'ERROR') !== false || $data === false) {
        if($data === false) $hash = 'ERROR: user not found';
		header('HTTP/1.1 401 ERR');
	    header('Content-Type: application/json; charset=UTF-8');
	    if (strrpos($hash,'ERROR: user not found') !== false)  die(json_encode(array('message' => 'Не найден пользователь', 'code' => 410)));
	    if (strrpos($hash,'duplicate') !== false)  die(json_encode(array('message' => 'Пользователь с таким имененм уже существует', 'code' => 411)));
        die(json_encode(array('message' => $hash, 'code' => 413)));
	
	}
    echo (json_encode($data));
    setcookie("chatuser", $name, time() + 3600);
    setcookie("chathash", $hash, time() + 3600);
} else {
	header('HTTP/1.1 401 COOKIE NOT FOUND');
}	
?>