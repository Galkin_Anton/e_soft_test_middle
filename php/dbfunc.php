<?php
error_reporting(E_ALL);
ini_set("log_errors", 1);
ini_set("error_log", "logs/php-error.log");
error_log( "Include dbfunc" );
set_time_limit(0);

/*connect*/
if (!$dbconn = pg_connect("host=".PGSERVER." port=5432 dbname=".PGDBNAME." user=".PGUSERNAME." password=".PGPASSWORD)) {
    //header('HTTP/1.1 500 Internal Server Booboo');
    //header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array('message' => 'Не удалось подключиться к серверу', 'code' => 1111)));
}
/**
* Вставляет нового пользователя
* @param name Имя пользователя
* @param password пароль
*/
function insert_user($name, $password) {
    global $dbconn;
    $result = pg_prepare($dbconn, "insert_user", 'INSERT INTO users ( u_name, u_pass, u_hash, u_color ) VALUES ( $1, $2, $3, $4 )');
    $passwd = md5($password);
    $hash = hash('whirlpool',microtime());
    $result = pg_execute($dbconn, "insert_user", array($name, $passwd, $hash, $color));
    //setcookie("chat", $hash, time()+3600);
    $err = pg_last_error($dbconn);

    if ($err) return $err;

    return $hash;
    
}

/*
* Обновление пользователя
*/
function update_user_by_hash($name, $hash, $color) {
    global $dbconn;
    
    $usr = get_user_by_hash($name, $hash);
    //если не нашли выводим ошибку
    if (!$usr) return "ERROR: user not found";

    $new_hash = hash('whirlpool',microtime());
    $result = pg_query_params($dbconn, 'UPDATE USERS SET U_HASH = $1, U_COLOR = $4 where U_NAME = $2 and U_HASH = $3', array($new_hash, $name, $hash, $color));
    //setcookie("chat", $hash, time()+3600);
    $err = pg_last_error($dbconn);
    if ($err) return $err;
    return $new_hash;
}
/**
* Получаем пользователя с хэшем
*/
function get_user_by_hash($name, $hash) {
    global $dbconn;
    $result = pg_query_params($dbconn, 'SELECT * FROM USERS where U_NAME = $1 and U_HASH = $2', array($name, $hash));
    //нам нужен только первый
    //return pg_fetch_all($result);
    return pg_fetch_assoc($result);
}
/**
* Получаем пользователя по имени
*/
function get_user_by_name($name) {
    global $dbconn;
    $result = pg_query_params($dbconn, 'SELECT * FROM USERS where U_NAME = $1', array($name));
    return pg_fetch_assoc($result);
}
/*
* Обновление пользователя
*/
function update_user_by_passwd($name, $passwd, $color) {
    global $dbconn;
    
    $usr = get_user_by_pass($name, $passwd);
    //если не нашли выводим ошибку
    if (!$usr) return "ERROR: user not found";

    $new_hash = hash('whirlpool',microtime());
    $result = pg_query_params($dbconn, 'UPDATE USERS SET U_HASH = $1, U_COLOR = $4 where U_NAME = $2 and U_PASS = $3', array($new_hash, $name, md5($passwd), $color));
    //setcookie("chat", $hash, time()+3600);
    $err = pg_last_error($dbconn);
    if ($err) return $err;
    return $new_hash;
}
/**
* Получаем пользователя с паролем
*/
function get_user_by_pass($name, $passwd) {
    global $dbconn;
    $result = pg_query_params($dbconn, 'SELECT * FROM USERS where U_NAME = $1 and U_PASS = $2', array($name, md5($passwd)));
    //нам нужен только первый
    //return pg_fetch_all($result);
    return pg_fetch_assoc($result);

}

/**
* Вставляем сообщение
* @param message сообщение
* @param user_id идентификатор пользователя
* @param room_id идентификатор группы
* @param m_date_message дата сообщения
* @param m_user_for идентификатор пользователя для которого сообщение
*/
function insert_message($message, $user_id, $room_id, $m_date_message, $m_user_for) {
    global $dbconn;
    
    $user = null;
    
    if($m_user_for) {
        $users = get_user_by_name($m_user_for);
        $user = $users['u_id'];
    }

    $result = pg_prepare($dbconn, "insert_message", 
    "INSERT INTO messages( m_message, m_user_id, m_room_id, m_date_message, m_user_for) 
    VALUES ($1, $2, $3, $4, $5)");
    $result = pg_execute($dbconn, "insert_message", array($message, $user_id, $room_id, $m_date_message, $user));
}

/*Выгружаем сообщения*/
function get_messages($limit) {
    global $dbconn;
    $result = pg_query_params($dbconn, 'select color, name, message, u.u_name as "userFor" from (select u_color color, m_id, m_user_for,m_message as message, u.u_name as name  from messages, users as u
            where u.u_id = m_user_id
            order by m_id desc limit $1
            ) as t
            left join users u on u_id = m_user_for    
    ', array($limit));
    return pg_fetch_all($result);
}
?>