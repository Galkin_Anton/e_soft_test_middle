<?php
//header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("log_errors", 1);
ini_set("error_log", "logs/login-error.log");
include "settings/db.php";
include "php/dbfunc.php";

if (!$_POST['name'] || !$_POST['password']) {
	header('HTTP/1.1 400 Where data');
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array('message' => 'Не переданы данные', 'code' => 400)));
}

if (isset($_POST) && !$_POST['isNew']) {

	$hash = update_user_by_passwd($_POST[name], $_POST['password'], $_POST['color']);

} elseif ($_POST['isNew']) {

	$hash = insert_user($_POST[name], $_POST['password'], $_POST['color']);

}
	if (strrpos($hash, 'ERROR') !== false) {
	
		header('HTTP/1.1 401 ERR');
	    header('Content-Type: application/json; charset=UTF-8');
	    if (strrpos($hash,'ERROR: user not found') !== false)  die(json_encode(array('message' => 'Не найден пользователь', 'code' => 401)));
	    if (strrpos($hash,'duplicate') !== false)  die(json_encode(array('message' => 'Пользователь с таким имененм уже существует', 'code' => 402)));
	    die(json_encode(array('message' => $hash, 'code' => 403)));
	
	}
	echo (json_encode(array('name' => $_POST[name],'password'=>$_POST['password'], 'color'=>$_POST['color'],'hash'=>$hash)));
	setcookie("chatuser", $_POST[name], time() + 3600);
	setcookie("chathash", $hash, time() + 3600);
	setcookie("mescolor", str_replace('#', '', $_POST['color']), time() + 3600);
?>