document.forms[0].addEventListener('submit', function(e) {
    e.preventDefault();
    $('.error')[0].style.display = 'none';
    var formData = new FormData(this);
    var o = {};
    for (var e of formData.entries()) {
        o[e[0]] = e[1];
    }

    $.ajax({
        type: "POST",
        url: 'login.php',
        data: formData,
        xhrFields: {
            withCredentials: true
        },
        success: function(d) {
            console.log(d);
            window.location = '/';
        },
        error: function(e) {
            var e = JSON.parse(e.responseText);
            $('.error')[0].style.display = 'block';
            $('.error').html(e.message);
            console.log(e.message);
        },
        contentType: false,
        processData: false,
        dataType: 'json'
    });

});

$('#color').on('change', function(e) {
    console.log(this.value);
    $('label[for="color"]')[0].style.background = this.value;
});