'use strict'
window.location = '/login.html';
/**
 * 
 * @param {*} name Имя пользователя
 * @param {*} color цвет фона сообщений
 */
function Message(name, color) {
    this.name = name;
    this.color = color;
    this.type = 'login';
    this.message = name + ' зашел';
    this.dateTime = new Date();
    this.userId = null;
    this.roomId = null;
    this.userFor = null;
}

Message.prototype.getSendMessage = function() {
    this.dateTime = new Date();
    return JSON.stringify(this);
};

var sendMessage = new Message('Galkin_AB', 'green');
//test
sendMessage.userId = 7;
/*------------------------------------------------*/

/*Запись сообщения*/

function addMessage(reciveMessage) {

    var msg = document.getElementById('msgs');
    var message = document.createElement('div');
    var messageInnder = document.createElement('div');
    msg.appendChild(message);
    message.appendChild(messageInnder);

    messageInnder.classList.add('message');
    messageInnder.innerHTML = reciveMessage.name + ': ' + reciveMessage.message;
    messageInnder.style.backgroundColor = reciveMessage.color;

    // var message,
    //     textAlign;
    // switch (key) {
    //     case value:

    //         break;

    //     default:
    //         break;
    // } 

    if (sendMessage.name === reciveMessage.name) message.style.textAlign = 'right';
    if (reciveMessage.type === 'system') {
        message.style.textAlign = 'center';
        messageInnder.innerHTML = reciveMessage.message;
    }

    msg.scrollTop = msg.scrollHeight;
}

/*Забираем сообщение с формы*/
var form = document.forms[0];
form.addEventListener('submit', function(e) {
    e.preventDefault();

    sendMessage.message = this['message'].value;

    //отправляем сообщение
    socket.send(sendMessage.getSendMessage());
    //обнуляю форму ввода
    this.reset();
});

//Socket
var socket = new WebSocket("ws://192.168.16.129:8001");

//при открытии отправляем сообщение
socket.onopen = function(m) {
    socket.send(sendMessage.getSendMessage());
};

//принимаем и печатаем сообщения
socket.onmessage = function(e) {
    console.log(e.data);

    addMessage(JSON.parse(e.data));
};

socket.onclose = function(event) {
    if (event.wasClean) {
        alert('Соединение закрыто чисто');
    } else {
        alert('Обрыв соединения'); // например, "убит" процесс сервера
    }
    alert('Код: ' + event.code + ' причина: ' + event.reason);
};