//window.location = '/login.html';
/**
 * 
 * @param {*} name Имя пользователя
 * @param {*} color цвет фона сообщений
 */
function Message(name, color, id) {
    this.name = name;
    this.color = color;
    this.type = 'login';
    this.message = name + ' зашел';
    this.dateTime = new Date();
    this.userId = id;
    this.roomId = null;
    this.userFor = null;
}

Message.prototype.getSendMessage = function() {
    this.dateTime = new Date();
    return JSON.stringify(this);
};

//var sendMessage = new Message('Galkin_AB', 'green');
//test
//sendMessage.userId = 7;
/*------------------------------------------------*/

/**
 * Проверяем пользователя залогинен он или нет
 */
var sendMessage;

/*работа с чатом*/
/*Запись сообщения*/

function addMessage(reciveMessage) {

    var msg = document.getElementById('msgs');
    var message = document.createElement('div');
    var messageInnder = document.createElement('div');
    msg.appendChild(message);
    message.appendChild(messageInnder);

    messageInnder.classList.add('message');
    messageInnder.innerHTML = reciveMessage.name + ': ' + reciveMessage.message;
    messageInnder.style.backgroundColor = reciveMessage.color;
    switch (sendMessage.name) {
        case reciveMessage.name:
            message.style.textAlign = 'right';
            break;
        case reciveMessage.userFor:
            message.style.textAlign = 'center';
            break
        default:
            message.style.textAlign = 'left';
            break;
    }
    msg.scrollTop = msg.scrollHeight;
}




/*Для начала грузим данные о пользователе при загрузке страницы*/
var formData = new FormData();
formData.append('action', 'get_user');

$.ajax({
    type: "POST",
    url: 'chat.php',
    data: formData,
    xhrFields: {
        withCredentials: true
    },
    success: function(d) {
        sendMessage = new Message(d.u_name, d.u_color, d.u_id);
        $('#loader')[0].style.display = 'none';
        main();
    },
    error: function(e) {
        console.error(e.responseText);
        window.location = '/login.html';
    },
    contentType: false,
    processData: false,
    dataType: 'json'
});


/*Функция обновления данных о пользователе*/
function checkUser() {
    var formData = new FormData();
    formData.append('action', 'updateuser');
    formData.append('color', sendMessage.color);

    $.ajax({
        type: "POST",
        url: 'chat.php',
        data: formData,
        xhrFields: {
            withCredentials: true
        },
        error: function(e) {
            console.error(e.responseText);
            //window.location = '/';
        },
        contentType: false,
        processData: false,
        dataType: 'json'
    });
}

//Основные действия
function main() {

    /*Грузим историю сообщений, возьмем допустим 100*/
    $.ajax({
        type: "GET",
        url: 'getmessages.php',
        xhrFields: {
            withCredentials: true
        },
        success: function(d) {
            for (var i = d.length - 1; i >= 0; i--)
                addMessage(d[i]);
        },
        error: function(e) {
            console.error(e.responseText);
        },
        contentType: false,
        processData: false,
        dataType: 'json'
    });

    $(function() {
        /*Работа с кнопкой кому*/
        $('form a').on('click', function(e) {
            $(this).css('display', 'none');
            sendMessage.userFor = null;
            return false;
        });

    });
    /*Добавить пользователя для шепота*/
    function whispUser(user) {
        $('form a').click();
        $('form a').text(user);
        if (user != sendMessage.name) {
            $('form a').css('display', 'block');
            sendMessage.userFor = user;
        }
    }

    /*Добавление пользователя в список*/
    function addUserInRoom(user) {

        var flag = false;
        var li = document.querySelectorAll('#rooms ul li');

        li.forEach(function(e) {
            if (e.textContent.indexOf(user) != -1) flag = true;
        });

        /*Добавляем*/
        if (flag === false) {
            li = document.createElement('li');
            var a = document.createElement('a');
            a.innerHTML = user;
            li.appendChild(a);
            a.onclick = function() { whispUser(user) };
            document.querySelectorAll('#rooms ul')[0].appendChild(li);
        }

    }
    /*Обновляем список пользователей*/
    function updateListUsers(users) {
        users.forEach(function(e) {
            addUserInRoom(e);
        });
    }
    /*удаление из списка пользователя*/
    function deleteUserFromList(user) {
        $('#rooms ul li').each(function() {
            if ($(this).text().indexOf(user) != -1) {
                $(this).remove();
            }
        });
    }
    /*Забираем сообщение с формы*/
    var form = document.forms[0];
    form.addEventListener('submit', function(e) {
        e.preventDefault();
        //Перед отправкой проверяем пользователя
        checkUser();
        sendMessage.message = this['message'].value;

        //отправляем сообщение
        sendMessage.type = 'usermsg';
        socket.send(sendMessage.getSendMessage());
        //обнуляю форму ввода
        this.reset();
    });

    //Socket
    var socket = new WebSocket("ws://" + window.location.host + ":8001");

    //при открытии отправляем сообщение
    socket.onopen = function(m) {
        sendMessage.type = 'in';
        sendMessage.message = 'Всем привет!';
        socket.send(sendMessage.getSendMessage());
    };

    //принимаем и печатаем сообщения
    socket.onmessage = function(e) {

        var msg = JSON.parse(e.data)
        switch (msg.type) {
            case 'in':
                addUserInRoom(msg.name);
                break;
            case 'usermsg':
                addMessage(msg);
                break;
            case 'all':
                updateListUsers(JSON.parse(msg.message)); //список сообщений
                break;
            case 'out':
                deleteUserFromList(msg.name); //пользователь который вышел из чата
            default:
                null;
                break;
        }

    };

    socket.onclose = function(event) {
        if (event.wasClean) {
            alert('Соединение закрыто чисто');
        } else {
            alert('Обрыв соединения');
        }
        alert('Код: ' + event.code + ' причина: ' + event.reason);
    };
}